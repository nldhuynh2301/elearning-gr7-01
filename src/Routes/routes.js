import SecureView from 'HOC/SecureView'
import AddUserPage from 'pages/AdminPage/AddUserPage'
import EditUserPage from 'pages/AdminPage/EditUserPage'
import Homepage from 'pages/HomePage'
import LoginPage from 'pages/LoginPage'
import ManagementPage from 'pages/AdminPage/ManagementPage'
import SignUpPage from 'pages/SignUpPage'
import AddCoursePage from 'pages/AdminPage/AddCoursePage'
import EditCoursePage from 'pages/AdminPage/EditCoursePage'
import DetailPage from 'pages/DetailPage/DetailPage'
import CourseCatalogPage from './../pages/HomePage/CourseCatalog/CourseCatalogPage'
import ThongTinTaiKhoan from 'pages/HomePage/ThongTinTaiKhoan/ThongTinTaiKhoan'
import RegisterCoursePage from 'pages/AdminPage/RegisterCourse/RegisterCoursePage'
import RegisterUserPage from 'pages/AdminPage/RegisterUser'

export const routes = [
  {
    path: '/',
    component: <Homepage />,
  },
  {
    path: '/detail/:id',
    component: <DetailPage />,
  },
  {
    path: '/course-catalog/:id',
    component: <CourseCatalogPage />,
  },
  {
    path: '/login',
    component: <LoginPage />,
  },
  {
    path: '/sign-up',
    component: <SignUpPage />,
  },
  {
    path: '/admin',
    component: (
      <SecureView>
        <ManagementPage />
      </SecureView>
    ),
  },
  {
    path: '/add-user',
    component: <AddUserPage />,
  },
  {
    path: '/edit-user/:taiKhoan',
    component: <EditUserPage />,
  },
  {
    path: '/register-user/:maKhoaHoc',
    component: <RegisterUserPage />,
  },
  {
    path: '/add-course',
    component: <AddCoursePage />,
  },
  {
    path: '/edit-course/:maKhoaHoc',
    component: <EditCoursePage />,
  },
  {
    path: '/ThongTinTaiKhoan',
    component: <ThongTinTaiKhoan />,
  },
  {
    path: '/register-course/:taiKhoan',
    component: <RegisterCoursePage />,
  },
]
