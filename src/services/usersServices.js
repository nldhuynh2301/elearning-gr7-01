import axios from 'axios'
import { userInforLocal } from './local.service'
import { BASE_URL, configHeader, https, TOKEN_CYBERSOFT } from './url.config'

export const userServ = {
  postLogin: (dataLogin) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
      method: 'POST',
      data: dataLogin,
      headers: configHeader(),
    })
  },
  postSignUp: (dataSignup) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangKy`,
      method: 'POST',
      data: dataSignup,
      headers: configHeader(),
    })
  },
  getUserList: () => {
    let uri = '/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP01'
    return https.get(uri)
  },
  deleteUser: (taiKhoan) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`,
      method: 'DELETE',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
    })
  },

  getUserForEditPage: (taiKhoan) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/ThongTinTaiKhoan`,
      method: 'POST',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
      data: {
        taiKhoan: taiKhoan,
      },
    })
  },
  updateUserInfor: (data) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung`,
      method: 'PUT',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
      data,
    })
  },
  addUserAccount: (data) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/ThemNguoiDung`,
      method: 'POST',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
      data,
    })
  },
  updateAccountInfor: (data) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung`,
      method: 'PUT',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
      data,
    })
  },
  getInforUser: (taiKhoan) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/ThongTinTaiKhoan`,
      method: 'POST',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
      data: {
        TaiKhoan: taiKhoan,
      },
    })
  },
  updateUserInfor2: (data) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung`,
      method: 'PUT',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
      data,
    })
  },
  getCourseNotRegisted: (taiKhoan) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/LayDanhSachKhoaHocChuaGhiDanh`,
      method: 'POST',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
      data: {
        taiKhoan: taiKhoan,
      },
    })
  },
  getCourseRegisted: (taiKhoan) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/LayDanhSachKhoaHocDaXetDuyet`,
      method: 'POST',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
      data: {
        taiKhoan: taiKhoan,
      },
    })
  },
  getCourseWaitingApprove: (taiKhoan) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/LayDanhSachKhoaHocChoXetDuyet`,
      method: 'POST',
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
      },
      data: {
        taiKhoan: taiKhoan,
      },
    })
  },
}
