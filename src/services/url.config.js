import axios from 'axios'
import { store } from '../redux/store/store'
import { turnOffLoading, turnOnLoading } from '../redux/slice/spinnerSlice'
import { userInforLocal } from './local.service'
export const TOKEN_CYBERSOFT =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzM0UiLCJIZXRIYW5TdHJpbmciOiIyNi8wNC8yMDIzIiwiSGV0SGFuVGltZSI6IjE2ODI0NjcyMDAwMDAiLCJuYmYiOjE2NTQzNjIwMDAsImV4cCI6MTY4MjYxNDgwMH0.GGqFf8-ZXIqAjnaJZ40LjQvUHb1VyvRv3XtEIsMe_qE'

export const BASE_URL = 'https://elearning0706.cybersoft.edu.vn'

export const configHeader = () => {
  return {
    TokenCyberSoft: TOKEN_CYBERSOFT,
  }
}

export const https = axios.create({
  baseURL: BASE_URL,
  headers: {
    TokenCybersoft: TOKEN_CYBERSOFT,
    Authorization: 'Bearer ' + userInforLocal.get()?.accessToken,
  },
})

https.interceptors.request.use(
  function (config) {
    store.dispatch(turnOnLoading())
    return config
  },
  function (error) {
    return Promise.reject(error)
  }
)

https.interceptors.response.use(
  function (response) {
    store.dispatch(turnOffLoading())
    return response
  },
  function (error) {
    return Promise.reject(error)
  }
)
