import React from 'react'
import ModalVideo from 'react-modal-video'
import { useDispatch, useSelector } from 'react-redux'
import { closeModalTrailer } from 'redux/slice/modalSlice'
import '../../../node_modules/react-modal-video/scss/modal-video.scss'
export default function ModalTrailer() {
  const dispatch = useDispatch()

  const isOpen = useSelector((state) => state.modalSlice.modalTrailer.isOpen)

  const videoId = useSelector((state) => state.modalSlice.modalTrailer.videoId)
  return (
    <ModalVideo
      channel="youtube"
      autoplay 
      isOpen={isOpen}
      videoId={videoId}
      onClose={() => dispatch(closeModalTrailer())}
    />
  )
}
