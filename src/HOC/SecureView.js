import { useEffect } from 'react'
import { userInforLocal } from 'services/local.service'

export default function SecureView({ children }) {
  useEffect(() => {
    let user = userInforLocal.get()
    if (user === null || user?.maLoaiNguoiDung !== 'GV') {
      window.location.href = '/login'
      userInforLocal.remove()
    }
  }, [])
  return children
}
