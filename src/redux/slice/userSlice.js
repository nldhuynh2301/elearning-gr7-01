const { createSlice } = require('@reduxjs/toolkit')

let initialState = {
  isLogin: true,
  userInfor: null,
  isRegisted: true,
}

const userSlice = createSlice({
  name: 'userSlice',
  initialState,
  reducers: {
    setUserInfor: (state, action) => {
      state.userInfor = action.payload
    },
    setIsRegisted: (state, action) => {
      state.isRegisted = action.payload
    },
  },
})

export const { setUserInfor } = userSlice.actions
export const { setIsRegisted } = userSlice.actions

export default userSlice.reducer
