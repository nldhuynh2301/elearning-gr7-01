import { configureStore } from '@reduxjs/toolkit'
import userSlice from '../slice/userSlice'
import modalSlice from '../slice/modalSlice'
import spinnerSlice from '../slice/spinnerSlice'

export const store = configureStore({
  reducer: {
    userSlice,
    modalSlice,
    spinnerSlice,
  },
})
