import '../node_modules/react-modal-video/css/modal-video.css'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
// import Navbar from "./components/Navbar";
import ModalTrailer from './components/ModalTrailer'
import { routes } from './Routes/routes'
import { nanoid } from 'nanoid'
import Spinner from './components/Spinner'
import Footer from 'components/Footer/Footer'
import Header from 'components/Header/Header'

export default function App() {
  return (
    <div className="App">
      <Spinner />
      <BrowserRouter>
        <Header />
        <Routes forceRefresh>
          {routes.map(({ path, component }) => {
            return <Route key={nanoid()} path={path} element={component} />
          })}
        </Routes>
        <ModalTrailer />
        <Footer />
      </BrowserRouter>
    </div>
  )
}
