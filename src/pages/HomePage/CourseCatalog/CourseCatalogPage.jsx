import React, { useEffect, useState } from 'react'
import { coursesServices } from './../../../services/coursesServices'
import { useParams } from 'react-router-dom'
import CourseCatalogItem from './CourseCatalogItem'
import styleSlick from './MultipleRowSlick.module.css'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import Slider from 'react-slick'

export default function CourseCatalogPage() {
  const [courseByCatalog, setCourseByCatalog] = useState([])
  let params = useParams()
  let { id } = params
  const fetchData = async () => {
    try {
      let res = await coursesServices.getCoursesByCategory({ maDanhMuc: id })
      setCourseByCatalog(res.data)
    } catch (error) {}
  }
  useEffect(() => {
    fetchData()
  }, [])
  function SampleNextArrow(props) {
    const { className, style, onClick } = props
    return (
      <div
        className={`${className} ${styleSlick['slick-next']}`}
        style={{
          ...style,
          display: 'block',
          color: 'black',
          // backgroundColor: "green",
        }}
        onClick={onClick}
      ></div>
    )
  }

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props
    return (
      <div
        className={`${className} ${styleSlick['slick-prev']}`}
        style={{ ...style, display: 'block', left: '-50px' }}
        onClick={onClick}
      ></div>
    )
  }

  const settings = {
    className: 'center',
    centerMode: false,
    infinite: true,
    centerPadding: '10px',
    slidesToShow: 3,
    speed: 400,
    rows: 2,
    slidesPerRow: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  }
  return (
    <div>
      <section className="text-gray-600 body-font">
        <div className="container px-5 py-24 mx-auto">
          <h1 className="text-left text-2xl font-bold p-4">
            Các khoá học phổ biến
          </h1>
          {/* <div className="flex flex-wrap -m-4"> */}
          <Slider {...settings}>
            {courseByCatalog.map((item, index) => {
              return <CourseCatalogItem key={index} item={item} />
            })}
          </Slider>

          {/* </div> */}
        </div>
      </section>
    </div>
  )
}
