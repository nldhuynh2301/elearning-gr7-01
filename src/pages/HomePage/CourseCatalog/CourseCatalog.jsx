import { nanoid } from 'nanoid'
import React, { useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom'
import { coursesServices } from '../../../services/coursesServices'
import './dropDown.css'

export default function CourseCatalog() {
  const [catalog, setCatalog] = useState([])
  const fetchData = async () => {
    try {
      const res = await coursesServices.getCourseCatalog()
      setCatalog(res.data)
    } catch (error) {}
  }
  useEffect(() => {
    fetchData()
  }, [])

  return (
    <div className="justify-center flex items-center">
      <div className="dropdown items-center text-white text-sm px-2 rounded-md">
        <button className="dropbtn  font-bold rounded-md">
          <i className="fa fa-bars" /> Danh mục khoá học
        </button>
        <div className="dropdown-content rounded-md">
          {catalog?.map((item) => {
            return (
              <div key={nanoid()}>
                <NavLink
                  reloadDocument
                  className="rounded-md"
                  to={`/course-catalog/${item.maDanhMuc}`}
                >
                  {item.tenDanhMuc}
                </NavLink>
              </div>
            )
          })}
        </div>
      </div>
    </div>
  )
}
