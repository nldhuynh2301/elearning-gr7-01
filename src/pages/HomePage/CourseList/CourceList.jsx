import React, { useEffect, useState } from 'react'
import { coursesServices } from 'services/coursesServices'
import CourceListItem from './CourceListItem'
import styleSlick from './MultipleRowSlick.module.css'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import Slider from 'react-slick'
export default function CourceList() {
  const [dataRaw, setDataRaw] = useState([])

  const fetchData = async () => {
    const params = {
      maNhom: 'GP01',
    }
    try {
      const res = await coursesServices.getCourseList(params)
      setDataRaw(res.data)
    } catch (error) {}
  }

  useEffect(() => {
    fetchData()
  }, [])
  function SampleNextArrow(props) {
    const { className, style, onClick } = props
    return (
      <div
        className={`${className} ${styleSlick['slick-next']}`}
        style={{
          ...style,
          display: 'block',
          color: 'black',
          // backgroundColor: "green",
        }}
        onClick={onClick}
      ></div>
    )
  }

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props
    return (
      <div
        className={`${className} ${styleSlick['slick-prev']}`}
        style={{ ...style, display: 'block', left: '-50px' }}
        onClick={onClick}
      ></div>
    )
  }

  const settings = {
    className: 'center',
    centerMode: false,
    infinite: true,
    centerPadding: '10px',
    slidesToShow: 3,
    speed: 400,
    rows: 2,
    slidesPerRow: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  }
  return (
    <div className="sm:w-full rounded-xl">
      <section className="text-gray-600 body-font ">
        <div className="container px-5 pb-24 mx-auto ">
          <h1 className="text-left text-2xl font-bold p-4">
            Các khoá học mới nhất
          </h1>
          <Slider {...settings}>
            {dataRaw?.map((item1, index) => {
              return <CourceListItem key={index} item={item1} />
            })}
          </Slider>
        </div>
      </section>
    </div>
  )
}
