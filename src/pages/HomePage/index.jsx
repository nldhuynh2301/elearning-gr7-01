import React from 'react'
import Carousel from './Carousel/Carousel'
import CourceList from './CourseList/CourceList'

export default function HomePage() {
  return (
    <div>
      <Carousel />
      <CourceList />
    </div>
  )
}
