import React from 'react'
import './carousel.css'
import { CaretRightOutlined } from '@ant-design/icons'
import { useDispatch } from 'react-redux'
import { openModalTrailer } from 'redux/slice/modalSlice'
import { NavLink } from 'react-router-dom'

export default function Carousel() {
  const dispatch = useDispatch()
  const videoID = '686mNAJVXzA'
  return (
    <div
      className={'styleall object-cover'}
      style={{
        backgroundImage: `url(https://cybersoft.edu.vn/wp-content/uploads/2019/02/cybersoft-lap-trinh-.jpg)`,
        height: '90vh',
        width: '100wh',
      }}
    >
      <div className={` style `}>
        <div className="grid grid-cols-12 mt-24">
          <div className="col-span-4 col-start-4">
            <div className="col-start-2 mt-32 text-white">
              <CaretRightOutlined
                onClick={() => {
                  dispatch(openModalTrailer(videoID))
                }}
                style={{ fontSize: 100 }}
              />
            </div>
          </div>
          <div className="col-span-3 col-start-8 text-white">
            <p className="text-5xl font-bold text-yellow-500">
              KHỞI ĐẦU SỰ NGHIỆP CỦA BẠN
            </p>
            <p className="text-2xl pt-4">
              Trở thành lập trình viên chuyên nghiệp tại CyberSoft
            </p>
            <div className="flex pt-3">
              <NavLink
                to={`/sign-up`}
                className="bg-yellow-500 hover:bg-slate-600 border-spacing-2 px-2 py-2 mr-4 "
              >
                ĐĂNG KÝ
              </NavLink>
              <button className="bg-yellow-500 hover:bg-slate-600 border-spacing-2 px-2 py-2  ">
                TƯ VẤN HỌC
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
