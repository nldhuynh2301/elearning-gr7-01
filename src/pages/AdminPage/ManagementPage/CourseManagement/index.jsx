import React, { useEffect, useState } from 'react'
import { message, Table, Button } from 'antd'
import _ from 'lodash'
import { Input } from 'antd'
import { NavLink } from 'react-router-dom'
import { createHeaderCoursesTable } from './courseUltis'
import { coursesServices } from 'services/coursesServices'
const { Search } = Input

export default function CourseManament() {
  const [dataRaw, setDataRaw] = useState([])
  const [memoryDataRaw, setMemoryDataRaw] = useState([])

  let handleDeleteCourse = (maKhoahoc) => {
    coursesServices
      .deleteCourse(maKhoahoc)
      .then((res) => {
        message.success('Xóa khóa học thành công')
        setTimeout(() => {
          fetchCoursesList()
        }, 1500)
      })
      .catch((err) => {
        message.error(err.response.data)
      })
  }

  let handleSearchCourse = (value) => {
    let specialCharacter =
      /[\`\~\!\@\#\%\&\>\<\:\;\,\_\=\"\'-.*+?^${}()|/[\]\\]{1,}/
    if (value.trim() === '') {
      setDataRaw(memoryDataRaw)
      return
    }
    if (specialCharacter.test(value.trim())) {
      setDataRaw([])
      return
    }
    let cloneDataRaw = _.cloneDeep(memoryDataRaw)
    let newData = [...cloneDataRaw].filter((item) => {
      return (
        item.tenKhoaHoc.toUpperCase().search(value.trim().toUpperCase()) !== -1
      )
    })
    if (newData.length !== 0) {
      setDataRaw(newData)
    } else {
      setDataRaw([])
    }
  }

  let fetchCoursesList = () => {
    coursesServices
      .getCourseList()
      .then((res) => {
        let dataRaw = res.data
        dataRaw.forEach((item, index) => {
          item.key = index + 1
        })
        setDataRaw(dataRaw)
        setMemoryDataRaw(dataRaw)
      })
      .catch((err) => {
        message.error(err.response.data)
      })
  }

  useEffect(() => {
    fetchCoursesList()
  }, [])

  return (
    <>
      <div className="flex justify-center">
        <Button className=" bg-amber-500 text-white px-10 rounded">
          <NavLink to="/add-course">Thêm Khóa học</NavLink>
        </Button>
      </div>
      <div className="px-10 py-10 ">
        <div className="flex space-x-5 justify-center items-center">
          <Search
            onSearch={handleSearchCourse}
            size="large"
            placeholder="Tìm kiếm Khóa học..."
            enterButton={
              <div className="border-0">
                <span className="text-black">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={24}
                    height={24}
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="#000000"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  >
                    <circle cx={11} cy={11} r={8} />
                    <line x1={21} y1={21} x2="16.65" y2="16.65" />
                  </svg>
                </span>
              </div>
            }
          />
          <Button
            onClick={() => {
              setDataRaw(memoryDataRaw)
            }}
          >
            Quay lại danh sách
          </Button>
        </div>
      </div>
      <Table
        dataSource={dataRaw}
        columns={createHeaderCoursesTable(handleDeleteCourse)}
        rowKey={(record) => record.key}
      />
    </>
  )
}
