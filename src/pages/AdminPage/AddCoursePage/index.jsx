import React, { useEffect, useState } from 'react'
import { coursesServices } from 'services/coursesServices'
import { CKEditor } from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import moment from 'moment'
import { useNavigate } from 'react-router-dom'
import { Button, DatePicker, Form, Input, message, Select } from 'antd'
import { userInforLocal } from 'services/local.service'
import { nanoid } from 'nanoid'
const { Option } = Select

export default function AddCoursePage() {
  let [image, setImage] = useState(null)
  let [courses, setCourses] = useState(null)
  let [currentUser, setCurrentUser] = useState(null)
  let [desc, setDesc] = useState(null)

  let navigate = useNavigate()

  const handleFileChange = (e) => {
    const file = e.target.files[0]
    let typeOfFile = file.type
    if (
      typeOfFile === 'image/png' ||
      typeOfFile === 'image/jpeg' ||
      typeOfFile === 'image/gif' ||
      typeOfFile === 'image/jpg'
    ) {
      let reader = new FileReader()
      reader.readAsDataURL(file)
      reader.addEventListener('load', (event) => {
        const img = event.target.result
        setImage(img)
      })
    }
  }

  const onFinish = async (values) => {
    let {
      maKhoaHoc,
      tenKhoaHoc,
      biDanh,
      luotXem,
      danhGia,
      ngayTao,
      maDanhMucKhoaHoc,
      taiKhoanNguoiTao,
    } = values

    let maNhom = currentUser.maNhom
    let moTa = desc

    let formData = new FormData()
    formData.append('maKhoaHoc', maKhoaHoc)
    formData.append('tenKhoaHoc', tenKhoaHoc)
    formData.append('biDanh', biDanh)
    formData.append('moTa', moTa)
    formData.append('luotXem', luotXem)
    formData.append('danhGia', danhGia)
    formData.append('maNhom', maNhom)
    formData.append('ngayTao', moment(ngayTao).format('DD/MM/YYYY'))
    formData.append('maDanhMucKhoaHoc', maDanhMucKhoaHoc)
    formData.append('taiKhoanNguoiTao', taiKhoanNguoiTao)
    let imageFile = document.getElementById('file').files[0]
    formData.append('File', imageFile, imageFile.name)

    try {
      coursesServices.addCourse(formData)
      message.success('Thêm khóa học thành công!')
      setTimeout(() => {
        navigate('/admin')
      }, 1500)
    } catch (err) {
      console.log('err: ', err)
      message.error(err.response.data)
    }
  }

  const fetchData = () => {
    coursesServices
      .getCourseCatalog()
      .then((res) => {
        setCourses(res.data)
      })
      .catch((err) => {
        message.error(err.response.data)
      })
  }

  useEffect(() => {
    let userInfor = userInforLocal.get()
    if (userInfor) {
      setCurrentUser(userInfor)
    }
    fetchData()
  }, [])

  return (
    <div className="container  px-10 py-32">
      <div className="border-4 border-red-500 py-5 bg-slate-500">
        <h1 className="text-3xl font-bold text-center mb-5">ADD COURSE</h1>
        <Form
          id="form"
          name="basic"
          labelCol={{
            span: 4,
          }}
          wrapperCol={{
            span: 14,
          }}
          layout="horizontal"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
        >
          <Form.Item name="maKhoaHoc" label="Mã khóa học">
            <Input />
          </Form.Item>
          <Form.Item name="biDanh" label="Bí danh">
            <Input />
          </Form.Item>
          <Form.Item name="tenKhoaHoc" label="Tên khóa học">
            <Input />
          </Form.Item>
          <Form.Item name="luotXem" label="Lượt xem">
            <Input />
          </Form.Item>
          <Form.Item name="danhGia" label="Đánh giá">
            <Input />
          </Form.Item>
          <Form.Item
            name="maDanhMucKhoaHoc"
            label="Danh mục khóa học"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select placeholder="Chọn khóa học" allowClear>
              {courses?.map((course) => {
                return (
                  <Option key={nanoid()} value={course.maDanhMuc}>
                    {course.tenDanhMuc}
                  </Option>
                )
              })}
            </Select>
          </Form.Item>
          <Form.Item name="taiKhoanNguoiTao" label="Người tạo">
            <Select placeholder="Người tạo" allowClear>
              <Option value={currentUser ? currentUser.taiKhoan : ''}>
                {currentUser?.hoTen}
              </Option>
            </Select>
          </Form.Item>
          <Form.Item name="ngayTao" label="Ngày tạo">
            <DatePicker />
          </Form.Item>

          <Form.Item label="Hình ảnh">
            <input
              style={{ background: 'transparent', border: 0 }}
              accept="image/png, image/jqg, image/gif, image/jpeg"
              type="file"
              id="file"
              onChange={(e) => {
                handleFileChange(e)
              }}
            />
          </Form.Item>
          <div>
            <img src={image} alt="" />
          </div>
          <Form.Item name="moTa" label="Mô tả">
            <CKEditor
              editor={ClassicEditor}
              data=""
              onChange={(event, editor) => {
                const data = editor.getData()
                let adjustData = data.slice(3, -4)
                setDesc(adjustData)
              }}
            />
          </Form.Item>
          <Form.Item label="Thêm phim">
            <Button htmlType="submit">Xác nhận</Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  )
}
