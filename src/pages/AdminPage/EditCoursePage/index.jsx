import React, { useEffect, useState } from 'react'
import { coursesServices } from 'services/coursesServices'
import { CKEditor } from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import moment from 'moment'
import { useNavigate, useParams } from 'react-router-dom'
import { Button, DatePicker, Form, Input, message, Select } from 'antd'
import { nanoid } from 'nanoid'
import { userInforLocal } from 'services/local.service'
const { Option } = Select

export default function EditCoursePage() {
  let [currentUser, setCurrentUser] = useState(null)
  let [image, setImage] = useState(null)
  let [course, setCourse] = useState(null)
  let [courseCatalog, setCourseCatalog] = useState(null)
  let [desc, setDesc] = useState(null)

  let params = useParams()
  let { maKhoaHoc } = params
  let navigate = useNavigate()

  const handleFileChange = (e) => {
    const file = e.target.files[0]
    let typeOfFile = file.type
    if (
      typeOfFile === 'image/png' ||
      typeOfFile === 'image/jpeg' ||
      typeOfFile === 'image/gif' ||
      typeOfFile === 'image/jpg'
    ) {
      let reader = new FileReader()
      reader.readAsDataURL(file)
      reader.addEventListener('load', (event) => {
        const img = event.target.result
        setImage(img)
      })
    }
  }

  const onFinish = (values) => {
    let {
      maKhoaHoc,
      tenKhoaHoc,
      luotXem,
      danhGia,
      ngayTao,
      maDanhMucKhoaHoc,
      taiKhoanNguoiTao,
    } = values
    let maNhom = course.maNhom
    let moTa = desc
    let frm = new FormData()

    let imageFile = document.getElementById('file').files[0]

    if (imageFile !== undefined) {
      let hinhAnh = imageFile.name
      console.log(' hinhAnh: ', hinhAnh)
      frm.append('file', imageFile)
      frm.append('tenKhoaHoc', tenKhoaHoc)

      let dataUpdate = {
        maKhoaHoc,
        tenKhoaHoc,
        moTa,
        luotXem,
        danhGia,
        hinhAnh,
        maNhom,
        ngayTao: moment(ngayTao).format('DD/MM/YYYY'),
        maDanhMucKhoaHoc,
        taiKhoanNguoiTao,
      }
      try {
        coursesServices.updateCourse(dataUpdate).then(() => {
          coursesServices.uploadImage(frm).catch((err1) => {
            message.error(err1.response.data)
          })
        })
        message.success('Cập nhật khóa học thành công!')
        setTimeout(() => {
          navigate('/admin')
        }, 1500)
      } catch (err2) {
        message.error(err2.response.data)
      }
    } else {
      alert('Vui lòng up ảnh từ máy sau đó thực hiện lại')
    }
  }

  const fetchData = () => {
    coursesServices
      .getDetailCourseAdmin(maKhoaHoc)
      .then((res) => {
        setCourse(res.data)
        setImage(res.data.hinhAnh)
      })
      .then(() => {
        coursesServices
          .getCourseCatalog()
          .then((res2) => {
            setCourseCatalog(res2.data)
          })
          .catch((err2) => {
            message.error(err2.response.data)
          })
      })
      .catch((err) => {
        message.error(err.response.data)
      })
  }

  useEffect(() => {
    let userInfor = userInforLocal.get()
    if (userInfor) {
      setCurrentUser(userInfor)
    }
    fetchData()
  }, [])

  const renderForm = () => {
    if (course !== null && courseCatalog !== null) {
      console.log('course: ', course)
      return (
        <Form
          initialValues={{
            maKhoaHoc: course.maKhoaHoc,
            tenKhoaHoc: course.tenKhoaHoc,
            biDanh: course.biDanh,
            moTa: course.moTa,
            hoTen: course.hoTen,
            luotXem: course.luotXem,
            maLoaiNguoiDung: course.maLoaiNguoiDung,
            ngayTao: moment(course.ngayTao),
            danhGia: 0,
            taiKhoanNguoiTao: currentUser.taiKhoan,
            maDanhMucKhoaHoc: course.danhMucKhoaHoc.maDanhMucKhoahoc,
          }}
          id="form"
          name="basic"
          labelCol={{
            span: 4,
          }}
          wrapperCol={{
            span: 14,
          }}
          layout="horizontal"
          onFinish={onFinish}
        >
          <Form.Item name="maKhoaHoc" label="Mã khóa học">
            <Input />
          </Form.Item>
          <Form.Item name="biDanh" label="Bí danh">
            <Input />
          </Form.Item>
          <Form.Item name="tenKhoaHoc" label="Tên khóa học">
            <Input />
          </Form.Item>
          <Form.Item name="luotXem" label="Lượt xem">
            <Input />
          </Form.Item>
          <Form.Item name="danhGia" label="Đánh giá">
            <Input />
          </Form.Item>
          <Form.Item
            name="maDanhMucKhoaHoc"
            label="Danh mục khóa học"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select placeholder="Chọn khóa học" allowClear>
              {courseCatalog.map((catalog) => {
                return (
                  <Option key={nanoid()} value={catalog.maDanhMuc}>
                    {catalog.tenDanhMuc}
                  </Option>
                )
              })}
            </Select>
          </Form.Item>
          <Form.Item name="taiKhoanNguoiTao" label="Người tạo">
            <Select placeholder="Người tạo" allowClear>
              <Option value={currentUser.taiKhoan}>{currentUser.hoTen}</Option>
            </Select>
          </Form.Item>
          <Form.Item name="ngayTao" label="Ngày tạo">
            <DatePicker />
          </Form.Item>

          <Form.Item label="Hình ảnh">
            <input
              style={{ background: 'transparent', border: 0 }}
              accept="image/png, image/jqg, image/gif, image/jpeg"
              type="file"
              id="file"
              onChange={(e) => {
                handleFileChange(e)
              }}
            />
          </Form.Item>
          <div>
            <img src={image} alt="" />
          </div>
          <Form.Item name="moTa" label="Mô tả">
            <CKEditor
              editor={ClassicEditor}
              data={course?.moTa}
              onChange={(event, editor) => {
                const data = editor.getData()
                let adjustData = data.slice(3, -4)
                setDesc(adjustData)
              }}
            />
          </Form.Item>
          <Form.Item label="Thêm khóa học">
            <Button htmlType="submit">Xác nhận</Button>
          </Form.Item>
        </Form>
      )
    }
  }
  return (
    <div className="container  px-10 py-32">
      <div className="border-4 border-red-500 py-5 bg-slate-500">
        <h1 className="text-3xl font-bold text-center mb-5">EDIT COURSE</h1>
        {renderForm()}
      </div>
    </div>
  )
}
