import React, { useEffect, useState } from 'react'
import { message } from 'antd'
import { userServ } from 'services/usersServices'
import { coursesServices } from 'services/coursesServices'
import { useParams } from 'react-router'

export default function ButtonRegisterCourse(props) {
  const [currentUser, setCurrentUser] = useState([])
  const params = useParams()
  let { taiKhoan } = params
  let fetchUserInfor = () => {
    userServ
      .getInforUser(taiKhoan)
      .then((res) => {
        setCurrentUser(res.data)
      })
      .catch((err) => {
        message.error(err.response.data)
      })
  }
  useEffect(() => {
    fetchUserInfor()
  }, [])
  let { maKhoaHoc } = props
  let dsKhoaHocGhiDanh = currentUser.chiTietKhoaHocGhiDanh
  const renderButtonRegisted = () => {
    let findIndex = (id, arr) => {
      for (let index = 0; index < arr?.length; index++) {
        var courseCurrent = arr[index]
        if (courseCurrent.maKhoaHoc == id) {
          return index
        }
      }
      return -1
    }
    let index = findIndex(maKhoaHoc, dsKhoaHocGhiDanh)
    return (
      <div className="absolute bottom-2 left-2">
        {index === -1 ? (
          <button
            onClick={handleRegisterCourse}
            className="font-bold text-white bg-green-500 hover:bg-slate-600 border-spacing-2 px-2 py-2 rounded-lg"
          >
            Đăng ký
          </button>
        ) : (
          <button
            onClick={handleCancelCourse}
            className="font-bold  text-white bg-red-500 hover:bg-slate-600 border-spacing-2 px-2 py-2 rounded-lg"
          >
            Hủy đăng ký
          </button>
        )}
      </div>
    )
  }

  const handleRegisterCourse = () => {
    let { taiKhoan } = params
    let registerData = {
      maKhoaHoc: maKhoaHoc,
      taiKhoan: taiKhoan,
    }

    coursesServices
      .postSignupCourse(registerData)
      .then((res) => {
        message.success('Hi! Bạn đã đăng ký khóa học thành công!')
        setTimeout(() => {
          window.location.reload()
        }, 1000)
      })
      .catch((err) => {
        message.error('Hi! Bạn ' + err.response.data)
        setTimeout(() => {
          window.location.reload()
        }, 1000)
      })
  }
  const handleCancelCourse = () => {
    let { taiKhoan } = params
    let registerData = {
      maKhoaHoc: maKhoaHoc,
      taiKhoan: taiKhoan,
    }

    coursesServices
      .postCancelCourse(registerData)
      .then((res) => {
        message.success('Hi! Bạn đã HỦY đăng ký khóa học thành công!')
        setTimeout(() => {
          window.location.reload()
        }, 1000)
      })
      .catch((err) => {
        message.error('Hi! Bạn ' + err.response.data)
        setTimeout(() => {
          window.location.reload()
        }, 1000)
      })
  }
  return <div>{renderButtonRegisted()}</div>
}
