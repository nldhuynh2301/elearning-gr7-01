import React, { useEffect, useState } from 'react'
import _ from 'lodash'
import { useParams } from 'react-router-dom'
import { message } from 'antd'
import { userServ } from 'services/usersServices'
import { nanoid } from 'nanoid'
import ListCourse from './ListCourse'

export default function ListCourseRegisted() {
  const [listCourseRegisted, setCourseListRegisted] = useState(null)
  const params = useParams()
  let { taiKhoan } = params

  let fetchCourseInfor = () => {
    userServ
      .getCourseRegisted(taiKhoan)
      .then((res) => {
        setCourseListRegisted(res.data)
      })
      .catch((err) => {
        message.error(err.response.data)
      })
  }

  useEffect(() => {
    fetchCourseInfor()
  }, [])
  let renderList = () => {
    if (listCourseRegisted) {
      return (
        <div className="grid grid-cols-4 gap-2">
          {listCourseRegisted.map((course) => {
            return <ListCourse key={nanoid()} item={course} />
          })}
        </div>
      )
    } else {
      return <div className="text-white">DS Trống</div>
    }
  }
  return <div>{renderList()}</div>
}
