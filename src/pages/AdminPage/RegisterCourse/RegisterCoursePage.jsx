import React, { useEffect, useState } from 'react'
import _ from 'lodash'
import { useParams } from 'react-router-dom'
import { Tabs, message } from 'antd'
import { userServ } from 'services/usersServices'

import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import ListCourseNotRegisted from './ListCourseNotRegisted'
import ListCourseRegisted from './ListCourseRegisted'
import ListCourseWaitingApprove from './ListCourseWaitingApprove'

export default function RegisterCoursePage() {
  const [currentUser, setCurrentUser] = useState(null)
  const params = useParams()
  let { taiKhoan } = params

  let fetchCourseInfor = () => {
    userServ
      .getCourseNotRegisted(taiKhoan)
      .then((res) => {
        setCurrentUser(res.data)
      })

      .catch((err) => {
        message.error(err.response.data)
      })
  }

  useEffect(() => {
    fetchCourseInfor()
  }, [])

  return (
    <div className="container  px-10 py-16">
      <div className="container border-4 border-red-500 py-5 bg-slate-500">
        <h1 className="text-3xl font-bold text-center mb-5">
          DANH SÁCH KHÓA HỌC
        </h1>
        <ListCourseNotRegisted />

        <hr />

        <p className="text-2xl font-bold text-white text-center mb-5">
          Danh sách khóa học đã được xét duyệt
        </p>
        <ListCourseRegisted />

        <hr />

        <p className="text-2xl font-bold text-white text-center mb-5">
          Danh sách khóa học chờ xét duyệt
        </p>
        <ListCourseWaitingApprove />
      </div>
    </div>
  )
}
