import { message } from 'antd'
import React from 'react'
import { NavLink, useParams } from 'react-router-dom'
import { coursesServices } from 'services/coursesServices'
import ButtonRegisterCourse from './ButtonRegisterCourse'

export default function ListCourseWaiting(props) {
  const params = useParams()
  let { tenKhoaHoc, maKhoaHoc } = props.item
  const handleApproveRegister = () => {
    let { taiKhoan } = params
    let approveData = {
      maKhoaHoc: maKhoaHoc,
      taiKhoan: taiKhoan,
    }
    coursesServices
      .approveCourseRegister(approveData)
      .then((res) => {
        message.success('Hi! Bạn đã xác thực ghi danh khóa học thành công!')
        setTimeout(() => {
          window.location.reload()
        }, 1000)
      })
      .catch((err) => {
        message.error('Hi! Bạn ' + err.response.data)
        setTimeout(() => {
          window.location.reload()
        }, 1000)
      })
  }
  return (
    <div className=" px-3 p-4 w-500 h-300 ">
      <div className=" h-100 bg-green-200 relative border-2 border-white-200 border-opacity-60 rounded-xl overflow-hidden ">
        <div className=" p-6 rounded-lg text-left   ">
          <h2 className="tracking-widest text-sm title-font font-medium text-green mb-1">
            Tên khoá Học
          </h2>
          <h1 className=" text-lg font-medium text-gray-900 mb-3">
            {tenKhoaHoc}
          </h1>
        </div>
        <NavLink
          to={`/detail/${maKhoaHoc}`}
          className=" text-red-500 absolute bottom-2 right-2 items-center "
        >
          <span className="px-1 mx-0">Chi tiết</span>
          <i className="fa-solid fa-square-up-right"></i>
        </NavLink>
        <button
          onClick={handleApproveRegister}
          className=" absolute bottom-14 left-2 font-bold text-white bg-green-500 hover:bg-slate-600 border-spacing-2 px-2 py-2 rounded-lg"
        >
          Xác thực ghi danh
        </button>
        <ButtonRegisterCourse maKhoaHoc={maKhoaHoc} />
      </div>
    </div>
  )
}
