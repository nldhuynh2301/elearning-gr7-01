import React, { useEffect, useState } from 'react'
import _ from 'lodash'
import { useParams } from 'react-router-dom'
import { coursesServices } from 'services/coursesServices'
import { Button, Form, Select, Table, Tabs, Input, message } from 'antd'
import { nanoid } from 'nanoid'
import { createHeaderUsersTable1 } from './userPendingCourseUltis'
import { createHeaderUsersTable2 } from './userAttendCourseUltis'
const { Search } = Input
const { Option } = Select

export default function RegisterUserPage() {
  let [userNotRegister, setUserNotRegister] = useState(null)

  const [dataRaw1, setDataRaw1] = useState([])
  const [memoryDataRaw1, setMemoryDataRaw1] = useState([])

  const [dataRaw2, setDataRaw2] = useState([])
  const [memoryDataRaw2, setMemoryDataRaw2] = useState([])

  const params = useParams()
  let { maKhoaHoc } = params

  let handleSearchUser1 = (value) => {
    let specialCharacter =
      /[\`\~\!\@\#\%\&\>\<\:\;\,\_\=\"\'-.*+?^${}()|/[\]\\]{1,}/
    if (value.trim() === '') {
      setDataRaw1(memoryDataRaw1)
      return
    }
    if (specialCharacter.test(value.trim())) {
      setDataRaw1([])
      return
    }
    let cloneDataRaw = _.cloneDeep(memoryDataRaw1)
    let newData = [...cloneDataRaw].filter((item) => {
      return item.hoTen.toUpperCase().search(value.trim().toUpperCase()) !== -1
    })
    if (newData.length !== 0) {
      setDataRaw1(newData)
    } else {
      setDataRaw1([])
    }
  }

  let handleSearchUser2 = (value) => {
    let specialCharacter =
      /[\`\~\!\@\#\%\&\>\<\:\;\,\_\=\"\'-.*+?^${}()|/[\]\\]{1,}/
    if (value.trim() === '') {
      setDataRaw2(memoryDataRaw2)
      return
    }
    if (specialCharacter.test(value.trim())) {
      setDataRaw2([])
      return
    }
    let cloneDataRaw = _.cloneDeep(memoryDataRaw2)
    let newData = [...cloneDataRaw].filter((item) => {
      return item.hoTen.toUpperCase().search(value.trim().toUpperCase()) !== -1
    })
    if (newData.length !== 0) {
      setDataRaw2(newData)
    } else {
      setDataRaw2([])
    }
  }

  let handleRegister = (taiKhoan) => {
    coursesServices
      .authenticateUserAttendCourseAdmin(maKhoaHoc, taiKhoan)
      .then((res) => {
        message.success('Xác thực ghi danh người dùng')
        setTimeout(() => {
          fetchData()
        }, 1500)
      })
      .catch((err) => {
        message.error(err.response.data)
      })
  }
  let handleCancelRegister = (taiKhoan) => {
    let registerData = {
      maKhoaHoc: maKhoaHoc,
      taiKhoan: taiKhoan,
    }
    coursesServices
      .postCancelCourse(registerData)
      .then((res) => {
        message.success('Hi! Bạn đã HỦY đăng ký khóa học thành công!')
        setTimeout(() => {
          fetchData()
        }, 1000)
      })
      .catch((err) => {
        message.error(err.response.data)
      })
  }

  const fetchData = () => {
    coursesServices
      .getUserNotRegisterAdmin(maKhoaHoc)
      .then((res) => {
        setUserNotRegister(res.data)
      })
      .then(() => {
        coursesServices
          .getUserCoursePendingAdmin(maKhoaHoc)
          .then((res) => {
            let newData = res.data
            newData.forEach((item, index) => {
              item.key = index + 1
            })
            setDataRaw1(newData)
            setMemoryDataRaw1(newData)
          })
          .then(() => {
            coursesServices
              .getUserCourseAttendAdmin(maKhoaHoc)
              .then((res) => {
                let newData = res.data
                newData.forEach((item, index) => {
                  item.key = index + 1
                })
                setDataRaw2(newData)
                setMemoryDataRaw2(newData)
              })
              .catch((err) => {})
          })
          .catch((err) => {})
      })
      .catch((err) => {})
  }

  const onFinish = (values) => {
    let { taiKhoan } = values

    let registerData = {
      maKhoaHoc: maKhoaHoc,
      taiKhoan: taiKhoan,
    }
    coursesServices
      .postSignupCourse(registerData)
      .then((res) => {
        message.success('Hi! Bạn đã đăng ký khóa học thành công!')
        setTimeout(() => {
          fetchData()
        }, 1000)
      })
      .catch((err) => {
        message.error(err.response.data)
      })
  }

  useEffect(() => {
    fetchData()
  }, [fetchData])

  return (
    <div className="container  px-10 py-16">
      <div className="border-4 border-red-500 py-5 bg-slate-500">
        <h1 className="text-3xl font-bold text-center mb-5">
          GHI DANH NGƯỜI DÙNG
        </h1>
        <Form
          id="form"
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 12,
          }}
          layout="horizontal"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
        >
          <div className="flex ">
            <div className="w-2/3">
              <Form.Item
                style={{ width: '100%' }}
                name="taiKhoan"
                label="Chọn người dùng"
                rules={[
                  {
                    required: true,
                  },
                ]}
              >
                <Select placeholder="Tên người dùng" allowClear>
                  {userNotRegister?.map((user) => {
                    return (
                      <Option key={nanoid()} value={user.taiKhoan}>
                        {user.hoTen}
                      </Option>
                    )
                  })}
                </Select>
              </Form.Item>
            </div>
            <div className="w-1/3">
              <Form.Item>
                <Button
                  className="bg-fuchsia-500 text-white  rounded"
                  htmlType="submit"
                >
                  Ghi danh
                </Button>
              </Form.Item>
            </div>
          </div>
        </Form>
        <hr />
        <Tabs tabPosition="top">
          <Tabs.TabPane
            tab={
              <p className="font-bold text-amber-500 uppercase px-5">
                Danh sách học viên chờ xác thực
              </p>
            }
            key="item-tab-1"
          >
            <div className="px-10 py-10">
              <div className="flex space-x-5 justify-center items-center">
                <Search
                  onSearch={handleSearchUser1}
                  size="large"
                  placeholder="Nhập tên học viên chờ xác thực..."
                  enterButton={
                    <div className="border-0">
                      <span className="text-black">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width={24}
                          height={24}
                          viewBox="0 0 24 24"
                          fill="none"
                          stroke="#000000"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        >
                          <circle cx={11} cy={11} r={8} />
                          <line x1={21} y1={21} x2="16.65" y2="16.65" />
                        </svg>
                      </span>
                    </div>
                  }
                />
                <div>
                  <Button
                    onClick={() => {
                      setDataRaw1(memoryDataRaw1)
                    }}
                  >
                    Quay lại danh sách
                  </Button>
                </div>
              </div>
            </div>
            <div className="px-10 py-10">
              <Table
                dataSource={dataRaw1}
                columns={createHeaderUsersTable1(
                  handleRegister,
                  handleCancelRegister
                )}
                rowKey={(record) => record.key}
              />
            </div>
          </Tabs.TabPane>
        </Tabs>
        <hr />
        <Tabs tabPosition="top">
          <Tabs.TabPane
            tab={
              <p className="font-bold text-amber-500 uppercase px-5">
                Danh sách học viên đã tham gia khóa học
              </p>
            }
            key="item-tab-1"
          >
            <div className="px-10 py-10">
              <div className="flex space-x-5 justify-center items-center">
                <Search
                  onSearch={handleSearchUser2}
                  size="large"
                  placeholder="Nhập tên học viên đã tham gia khóa học ..."
                  enterButton={
                    <div className="border-0">
                      <span className="text-black">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width={24}
                          height={24}
                          viewBox="0 0 24 24"
                          fill="none"
                          stroke="#000000"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        >
                          <circle cx={11} cy={11} r={8} />
                          <line x1={21} y1={21} x2="16.65" y2="16.65" />
                        </svg>
                      </span>
                    </div>
                  }
                />
                <div>
                  <Button
                    onClick={() => {
                      setDataRaw2(memoryDataRaw2)
                    }}
                  >
                    Quay lại danh sách
                  </Button>
                </div>
              </div>
            </div>
            <div className="px-10 py-10">
              <Table
                dataSource={dataRaw2}
                columns={createHeaderUsersTable2(handleCancelRegister)}
                rowKey={(record) => record.key}
              />
            </div>
          </Tabs.TabPane>
        </Tabs>
      </div>
    </div>
  )
}
